export const environment = {
  production: true,
  inMemoryWebApi: true,
  apiToken: null,
  apiUrl: null,
};
