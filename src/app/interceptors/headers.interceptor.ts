import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { environment } from '../../environments/environment';


@Injectable()
export class HeadersInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const headers: any = {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json',
    };
    if (environment.apiToken) {
      console.log(environment.apiToken);
      headers.Authorization = `Token ${environment.apiToken}`;
    }
    request = request.clone({
      setHeaders: headers
    });
    return next.handle(request);
  }
}
